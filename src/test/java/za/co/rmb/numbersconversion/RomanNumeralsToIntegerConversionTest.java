package za.co.rmb.numbersconversion;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Created by simiso on 2016/08/14.
 */
public class RomanNumeralsToIntegerConversionTest {
    @Test
    public void testIsValidRomanNumeral() {
        RomanNumeralsToIntegerConversion romanNumeralsToIntegerConversion = new RomanNumeralsToIntegerConversion();
        assertFalse(romanNumeralsToIntegerConversion.isValidRomanNumeral(""));
        assertFalse(romanNumeralsToIntegerConversion.isValidRomanNumeral("Simiso"));
        assertFalse(romanNumeralsToIntegerConversion.isValidRomanNumeral("VXII"));
        assertFalse(romanNumeralsToIntegerConversion.isValidRomanNumeral("VL"));
        assertFalse(romanNumeralsToIntegerConversion.isValidRomanNumeral("LCXVI"));
        assertFalse(romanNumeralsToIntegerConversion.isValidRomanNumeral("XVIC"));
        assertFalse(romanNumeralsToIntegerConversion.isValidRomanNumeral("MCMXICIX"));
        assertFalse(romanNumeralsToIntegerConversion.isValidRomanNumeral("MDDCLI"));
        assertFalse(romanNumeralsToIntegerConversion.isValidRomanNumeral("VIIII"));
        assertFalse(romanNumeralsToIntegerConversion.isValidRomanNumeral("CCCXXXXVIII"));
        assertTrue(romanNumeralsToIntegerConversion.isValidRomanNumeral("I"));
        assertTrue(romanNumeralsToIntegerConversion.isValidRomanNumeral("X"));
        assertTrue(romanNumeralsToIntegerConversion.isValidRomanNumeral("C"));
        assertTrue(romanNumeralsToIntegerConversion.isValidRomanNumeral("CLI"));
        assertTrue(romanNumeralsToIntegerConversion.isValidRomanNumeral("CXL"));
        assertTrue(romanNumeralsToIntegerConversion.isValidRomanNumeral("CXLVIII"));
        assertTrue(romanNumeralsToIntegerConversion.isValidRomanNumeral("MMMDCCCLXXXVIII"));
    }

    @Test
    public void testConvertRomanToInt() {
        RomanNumeralsToIntegerConversion romanNumeralsToIntegerConversion = new RomanNumeralsToIntegerConversion();
        assertEquals(1, romanNumeralsToIntegerConversion.convertRomanToInt("I"));
        assertEquals(2, romanNumeralsToIntegerConversion.convertRomanToInt("II"));
        assertEquals(3, romanNumeralsToIntegerConversion.convertRomanToInt("III"));
        assertEquals(4, romanNumeralsToIntegerConversion.convertRomanToInt("IV"));
        assertEquals(5, romanNumeralsToIntegerConversion.convertRomanToInt("V"));
        assertEquals(6, romanNumeralsToIntegerConversion.convertRomanToInt("VI"));
        assertEquals(7, romanNumeralsToIntegerConversion.convertRomanToInt("VII"));
        assertEquals(8, romanNumeralsToIntegerConversion.convertRomanToInt("VIII"));
        assertEquals(9, romanNumeralsToIntegerConversion.convertRomanToInt("IX"));
        assertEquals(10, romanNumeralsToIntegerConversion.convertRomanToInt("X"));
        assertEquals(369, romanNumeralsToIntegerConversion.convertRomanToInt("CCCLXIX"));
        assertEquals(449, romanNumeralsToIntegerConversion.convertRomanToInt("CDXLIX"));
        assertEquals(1998, romanNumeralsToIntegerConversion.convertRomanToInt("MCMXCVIII"));
        assertEquals(1999, romanNumeralsToIntegerConversion.convertRomanToInt("MCMXCIX"));
        assertEquals(2001, romanNumeralsToIntegerConversion.convertRomanToInt("MMI"));
    }

}
