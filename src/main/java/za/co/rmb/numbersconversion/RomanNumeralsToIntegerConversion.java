package za.co.rmb.numbersconversion;

import java.util.regex.Pattern;

/**
 * Created by simiso on 2016/08/14.
 */
public class RomanNumeralsToIntegerConversion {

    public int convertRomanToInt(String romanNumber) {
        romanNumber = romanNumber.toUpperCase();
        int intValue = 0;
        int previousRomanValue = 0;
        int currentRomanValue = 0;
        for (int i = romanNumber.length() - 1; i >= 0; i--) {
            currentRomanValue = RomanNumeralsToIntegerMapping.getRomanIntegerMapping(romanNumber.charAt(i));
            if (currentRomanValue >= previousRomanValue)
                intValue += currentRomanValue;
            else
                intValue -= currentRomanValue;
            previousRomanValue = currentRomanValue;
        }
        return intValue;
    }
    public boolean isValidRomanNumeral(String romanNumeral) {
        // Rule 1 : "I", "X", "C", and "M" can be repeated three times in succession, but no more. M{0,3}( C{0,3} ) ( X{0,3)) ...
        // RUle 2 :  "D", "L", and "V" can never be repeated. D{0,1} or D? ....
        // Rule 3 : "I" can be subtracted from "V" and "X" only. "X" can be subtracted from "L" and "C" only.
        //"C" can be subtracted from "D" and "M" only. "V", "L", and "D" can never be subtracted. CM|CD , XC|XL, and IX|IV
        // Rule 4 : Only one small-value symbol may be subtracted from any large-value symbol. Order the grouping from larger valued M ----> I ie M{0,3}( C{0,3} ) ( X{0,3))

        Pattern regex = Pattern.compile("M{0,3}(CM|CD|D{0,1}C{0,3})(XC|XL|L{0,1}X{0,3})(IX|IV|V{0,1}I{0,3})");
        return romanNumeral != null && !"".equals(romanNumeral) && regex.matcher(romanNumeral).matches();
    }
}
