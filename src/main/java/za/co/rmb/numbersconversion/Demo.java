package za.co.rmb.numbersconversion;

import java.io.*;
import java.util.*;

/**
 * Created by simiso on 2016/08/14.
 */
public class Demo {
    private final static int INVALID_ROMAN_NUMERAL = -2;
    private final static int HAS_NO_GALATIC_MAPPER = -1;
    private static String  fileName = "/opt/enviroment/development/projects/numbers-conversion/src/main/resources/Input.txt";


    public static void main(String[] args) {
        RomanNumeralsToIntegerConversion romanIntegerConversion = new RomanNumeralsToIntegerConversion();
        BufferedReader br = null;
        Map<String, Character> intergalacticMapper = new HashMap<String, Character>();
        Map<String, Double> mineralCredits = new HashMap<String, Double>();
        Deque<String> deque=null;

        int totalValue = 0;
        try {
            br = new BufferedReader(new FileReader(fileName));
            String line;
            Double creditValue = 0.0;
            String mineralType = "";

            while ((line = br.readLine()) != null) {
                deque = new ArrayDeque<String>();
                if (line.contains("how much is ") && line.contains("?")) {
                    line = line.replaceAll("how much is ", "");
                    line = line.replaceAll("\\?", "");
                    deque.addAll(Arrays.asList(line.split(" ")));
                    totalValue = getIntegerConversion(romanIntegerConversion, deque, intergalacticMapper);
                    if (totalValue > 0) {
                        print(deque);
                        System.out.println("is " + totalValue);
                    } else {
                        System.out.println("I have no idea what you are talking about");
                    }
                } else if (line.contains("how many Credits is ") && line.contains("?")) {
                    line = line.replaceAll("how many Credits is ", "");
                    line = line.replaceAll("\\?", "");
                    deque.addAll(Arrays.asList(line.split(" ")));
                    mineralType = deque.pollLast();
                    totalValue = getIntegerConversion(romanIntegerConversion, deque, intergalacticMapper);
                    if (totalValue > 0) {
                        if (mineralCredits.containsKey(mineralType)) {
                            print(deque);
                            System.out.println(mineralType + " is " + (int) (totalValue * mineralCredits.get(mineralType)) + " Credits ");
                        } else {
                            System.out.println("I have no idea what you are talking about");
                        }
                    } else {
                        System.out.println("I have no idea what you are talking about");
                    }

                } else {
                    deque.addAll(Arrays.asList(line.split(" ")));
                    String word = deque.pollLast();
                    if (word.length() == 1 && romanIntegerConversion.isValidRomanNumeral(word)) {
                        if (deque.pollLast().equals("is"))
                            intergalacticMapper.put(deque.pollLast(), word.charAt(0));
                    } else if (word.contains("Credits")) {
                        creditValue = Double.parseDouble(deque.pollLast());
                        if (deque.pollLast().equals("is")) {
                            mineralType = deque.pollLast();
                            totalValue = getIntegerConversion(romanIntegerConversion, deque, intergalacticMapper);
                            mineralCredits.put(mineralType, getUnitValueForMineral(totalValue, creditValue));
                        }
                    } else {
                        System.out.println("I have no idea what you are talking about");
                    }
                }
            }
        } catch (FileNotFoundException exeption) {
            System.out.println(exeption);
        } catch (IOException exeption) {
            System.out.println(exeption);
        } finally {
            try {
                br.close();
            } catch (IOException exeption) {
                System.out.println(exeption);
            }
        }
    }

    private static Double getUnitValueForMineral(int totalMinerals, Double creditValue) {
        return creditValue / totalMinerals;
    }

    private static int getIntegerConversion(final RomanNumeralsToIntegerConversion romanIntegerConversion,final Deque<String> deque, final Map intergalacticMapper) {
        String romanNumeral = "";
        for (String galaxyValue : deque) {
            if (intergalacticMapper.containsKey(galaxyValue)) {
                romanNumeral += intergalacticMapper.get(galaxyValue);
            } else {
                return HAS_NO_GALATIC_MAPPER;
            }
        }
        if (romanIntegerConversion.isValidRomanNumeral(romanNumeral))
            return romanIntegerConversion.convertRomanToInt(romanNumeral);
        else
            return INVALID_ROMAN_NUMERAL;
    }

    private static void print(Deque<String> deque) {
        for (String galaxyValue : deque) {
            System.out.print(galaxyValue + " ");
        }
    }
}