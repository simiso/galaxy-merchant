package za.co.rmb.numbersconversion;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by simiso on 2016/08/14.
 */
public class RomanNumeralsToIntegerMapping {

    private static  Map<Character,Integer> romanIntegerMapping =  new HashMap<Character, Integer>();

    static {
        romanIntegerMapping.put('I', 1);
        romanIntegerMapping.put('V', 5);
        romanIntegerMapping.put('X', 10);
        romanIntegerMapping.put('L', 50);
        romanIntegerMapping.put('C', 100);
        romanIntegerMapping.put('D', 500);
        romanIntegerMapping.put('M', 1000);
    }


    public static Integer getRomanIntegerMapping(char key)
    {
        return romanIntegerMapping.get(key);
    }

}
